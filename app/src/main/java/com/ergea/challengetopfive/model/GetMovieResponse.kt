package com.ergea.challengetopfive.model

import com.google.gson.annotations.SerializedName

data class GetMovieResponse(
    @SerializedName("results")
    val movies: List<DataMovie>
)