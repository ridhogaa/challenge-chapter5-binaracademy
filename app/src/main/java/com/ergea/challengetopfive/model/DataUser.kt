package com.ergea.challengetopfive.model

data class DataUser(
    val username: String?,
    val email: String?,
    val password: String?,
    val nama_lengkap: String? = "",
    val tanggal_lahir: String? = "",
    val alamat: String? = ""
)