package com.ergea.challengetopfive.utils

class Constants {
    companion object{
        const val PREFS_NAME = "MY_PREF_LOGIN_REGISTER"
        const val PREF_IS_LOGIN = "PREF_IS_LOGIN"
        const val PREF_USERNAME = "PREF_USERNAME"
        const val PREF_USER_ID = "PREF_USER_ID"
    }
}