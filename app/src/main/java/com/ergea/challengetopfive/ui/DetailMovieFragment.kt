package com.ergea.challengetopfive.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.ergea.challengetopfive.R
import com.ergea.challengetopfive.databinding.FragmentDetailMovieBinding
import com.ergea.challengetopfive.model.GetMovieDetailResponse
import com.ergea.challengetopfive.service.movie.APIMovie
import com.ergea.challengetopfive.viewmodel.ViewModelMovie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMovieFragment : Fragment() {
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: ViewModelMovie
    private val IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelMovie::class.java)
        val id = arguments?.getInt("ID")
        if (id != null) {
            response(id)
        }
    }

    private fun response(id: Int) {
        APIMovie.instance.getMovieDetail(id)
            .enqueue(object : Callback<GetMovieDetailResponse> {
                override fun onResponse(
                    call: Call<GetMovieDetailResponse>,
                    response: Response<GetMovieDetailResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody != null) {
                            Log.d(ContentValues.TAG, "onResponse: ${responseBody.toString()}")
                            binding.apply {
                                titleMovie.text = responseBody.title.toString()
                                releaseDataMovie.text = responseBody.releaseDate.toString()
                                Glide.with(requireContext())
                                    .load(IMAGE_BASE + responseBody.backdropPath)
                                    .into(binding.movieImage)
                                descMovie.text = responseBody.overview.toString()
                            }
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Movie Tidak Ditemukan",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<GetMovieDetailResponse>, t: Throwable) {

                }

            })
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}