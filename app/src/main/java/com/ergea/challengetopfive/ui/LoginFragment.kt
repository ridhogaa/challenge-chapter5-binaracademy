package com.ergea.challengetopfive.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.ergea.challengetopfive.R
import com.ergea.challengetopfive.databinding.FragmentLoginBinding
import com.ergea.challengetopfive.model.GetUserResponseItem
import com.ergea.challengetopfive.service.user.APIUser
import com.ergea.challengetopfive.utils.Constants
import com.ergea.challengetopfive.utils.PreferencesHelper
import com.ergea.challengetopfive.viewmodel.ViewModelUser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var userVM: ViewModelUser
    private lateinit var sharedPreferences: PreferencesHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        userVM = ViewModelProvider(this).get(ViewModelUser::class.java)
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toRegister()
        toLogin()
    }

    private fun toLogin() {
        binding.btnLogin.setOnClickListener {
            authentification(
                binding.etEmail.text.toString().trim(),
                binding.etPassword.text.toString().trim()
            )
        }
    }

    override fun onStart() {
        super.onStart()
        if (sharedPreferences.getBoolean(Constants.PREF_IS_LOGIN)) {
            findNavController().navigate(R.id.action_loginFragment_to_homeMovieFragment)
        }
    }

    private fun authentification(email: String, password: String) {
        APIUser.instance.getAllUser()
            .enqueue(object : Callback<List<GetUserResponseItem>> {
                override fun onResponse(
                    call: Call<List<GetUserResponseItem>>,
                    response: Response<List<GetUserResponseItem>>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody != null) {
                            Log.d(ContentValues.TAG, "onResponse: ${responseBody.toString()}")
                            for (i in responseBody.indices) {
                                if (responseBody[i].email.equals(
                                        email,
                                        ignoreCase = false
                                    ) && responseBody[i].password.equals(
                                        password, ignoreCase = false
                                    )
                                ) {
                                    sharedPreferences.put(
                                        Constants.PREF_USERNAME,
                                        responseBody[i].username.toString()
                                    )
                                    sharedPreferences.put(
                                        Constants.PREF_USER_ID,
                                        responseBody[i].id!!.toInt()
                                    )
                                    sharedPreferences.put(Constants.PREF_IS_LOGIN, true)
                                    Toast.makeText(
                                        requireContext(),
                                        "Login Berhasil",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    findNavController().navigate(R.id.action_loginFragment_to_homeMovieFragment)
                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        "Login Gagal",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<List<GetUserResponseItem>>, t: Throwable) {
                    Toast.makeText(requireContext(), "Something Wrong", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun toRegister() {
        binding.textBpa.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}