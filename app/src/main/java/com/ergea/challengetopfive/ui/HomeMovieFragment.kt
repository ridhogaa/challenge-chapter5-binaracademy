package com.ergea.challengetopfive.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ergea.challengetopfive.R
import com.ergea.challengetopfive.adapter.MovieAdapter
import com.ergea.challengetopfive.databinding.FragmentHomeMovieBinding
import com.ergea.challengetopfive.model.DataMovie
import com.ergea.challengetopfive.model.GetMovieResponse
import com.ergea.challengetopfive.service.movie.APIMovie
import com.ergea.challengetopfive.utils.Constants
import com.ergea.challengetopfive.utils.PreferencesHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeMovieFragment : Fragment() {
    private var _binding: FragmentHomeMovieBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedPreferences: PreferencesHelper
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentHomeMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.txtWelcomeUsername.text = "Hi, ${sharedPreferences.getString(Constants.PREF_USERNAME)}!"
        binding.movieRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.movieRecyclerView.setHasFixedSize(true)
        getMovieData { movies: List<DataMovie> ->
            binding.movieRecyclerView.adapter = MovieAdapter(movies)
        }
        binding.imgProfile.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeMovieFragment_to_profileFragment)
        }
    }

    private fun getMovieData(callback: (List<DataMovie>) -> Unit) {
        APIMovie.instance.getMovieList().enqueue(object : Callback<GetMovieResponse> {
            override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<GetMovieResponse>,
                response: Response<GetMovieResponse>
            ) {
                return callback(response.body()!!.movies)
            }

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}