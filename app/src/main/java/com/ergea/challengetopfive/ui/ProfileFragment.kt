package com.ergea.challengetopfive.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.ergea.challengetopfive.R
import com.ergea.challengetopfive.databinding.FragmentProfileBinding
import com.ergea.challengetopfive.utils.Constants
import com.ergea.challengetopfive.utils.PreferencesHelper
import com.ergea.challengetopfive.viewmodel.ViewModelUser
import kotlin.math.log

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var userVM: ViewModelUser
    private lateinit var sharedPreferences: PreferencesHelper
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userVM = ViewModelProvider(this).get(ViewModelUser::class.java)
        update()
        logout()
    }

    private fun update(){
        binding.btnUpdate.setOnClickListener {
            val username = binding.etUsername.text.toString().trim()
            val namaLengkap = binding.etNamaLengkap.text.toString().trim()
            val tanggalLahir = binding.etTanggalLahir.text.toString().trim()
            val alamat = binding.etAlamat.text.toString().trim()
            sharedPreferences.put(Constants.PREF_USERNAME, username)
            userVM.updatePutApiUser(sharedPreferences.getInt(Constants.PREF_USER_ID), username, namaLengkap, tanggalLahir, alamat)
            Toast.makeText(requireContext(), "Update Success", Toast.LENGTH_SHORT)
                .show()
            it.findNavController().navigate(R.id.action_profileFragment_to_homeMovieFragment)
        }
    }

    private fun logout(){
        binding.btnLogout.setOnClickListener {
            sharedPreferences.clear()
            Toast.makeText(requireContext(), "Logout", Toast.LENGTH_SHORT)
                .show()
            it.findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}