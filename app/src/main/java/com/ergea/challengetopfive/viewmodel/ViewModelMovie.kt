package com.ergea.challengetopfive.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ergea.challengetopfive.model.GetMovieDetailResponse
import com.ergea.challengetopfive.service.movie.APIMovie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModelMovie : ViewModel() {
    private val _getDetailMovieLiveData = MutableLiveData<GetMovieDetailResponse?>()
    val getDetailMovieLiveData: LiveData<GetMovieDetailResponse?> = _getDetailMovieLiveData


    fun getMovieDetailById(id: Int) {
        APIMovie.instance.getMovieDetail(id)
            .enqueue(object : Callback<GetMovieDetailResponse> {
                override fun onResponse(
                    call: Call<GetMovieDetailResponse>,
                    response: Response<GetMovieDetailResponse>
                ) {
                    if (response.isSuccessful) {
                        _getDetailMovieLiveData.value = response.body()
                    } else {
                        _getDetailMovieLiveData.value = null
                    }
                }

                override fun onFailure(call: Call<GetMovieDetailResponse>, t: Throwable) {

                }

            })
    }


}