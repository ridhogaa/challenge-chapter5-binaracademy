package com.ergea.challengetopfive.service.user

import com.ergea.challengetopfive.model.DataProfile
import com.ergea.challengetopfive.model.DataUser
import com.ergea.challengetopfive.model.GetUserResponseItem
import com.ergea.challengetopfive.model.PostUserResponse
import retrofit2.Call
import retrofit2.http.*

interface APIUserInterface {
    @GET("users")
    fun getAllUser(): Call<List<GetUserResponseItem>>

    @POST("users")
    fun registerUser(@Body request: DataUser): Call<PostUserResponse>

    @PUT("users/{id}")
    fun updateUser(@Path("id") id : Int, @Body request: DataProfile): Call<PostUserResponse>
}