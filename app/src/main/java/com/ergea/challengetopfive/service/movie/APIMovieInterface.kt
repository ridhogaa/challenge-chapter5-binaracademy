package com.ergea.challengetopfive.service.movie

import com.ergea.challengetopfive.model.GetMovieDetailResponse
import com.ergea.challengetopfive.model.GetMovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface APIMovieInterface {
    @GET("/3/movie/popular?api_key=9428967aca5607f7a2bbcb7a46f0ecfe")
    fun getMovieList(): Call<GetMovieResponse>

    @GET("/3/movie/{id}?api_key=9428967aca5607f7a2bbcb7a46f0ecfe")
    fun getMovieDetail(@Path("id") id: Int): Call<GetMovieDetailResponse>
}